﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entity
{
    public class LeaveRequest : EntityBase
    {
        public string StudentName { get; set; }
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public virtual Class Class { get; set; }
        public string Title { get; set; }
        public DateTime LeaveDate { get; set; }
        public int LeaveType { get; set; }
        public string ReasonLeave { get; set; }
        public int Status { get; set; }
    }
}
