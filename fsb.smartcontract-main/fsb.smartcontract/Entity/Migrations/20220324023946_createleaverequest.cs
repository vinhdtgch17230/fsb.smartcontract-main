﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class createleaverequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 24, 9, 39, 45, 753, DateTimeKind.Local).AddTicks(968),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "LeaveDate",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 24, 9, 39, 45, 752, DateTimeKind.Local).AddTicks(9073),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 24, 9, 39, 45, 753, DateTimeKind.Local).AddTicks(968));

            migrationBuilder.AlterColumn<string>(
                name: "LeaveDate",
                table: "LeaveRequest",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 24, 9, 39, 45, 752, DateTimeKind.Local).AddTicks(9073));
        }
    }
}
