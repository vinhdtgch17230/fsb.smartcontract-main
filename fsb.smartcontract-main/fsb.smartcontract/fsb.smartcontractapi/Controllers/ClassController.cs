﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Class;
using Model.Response;
using Repository.Class;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        private readonly IClassRepository _classRepository;

        public ClassController(
            IClassRepository classRepository
            ) 
        {
            _classRepository = classRepository;
        }

        [Route("CreateClass")]
        [HttpPost]
        //[CustomClass(ClassConstants.ADMIN)]
        public Response<string> CreateClass(ClassModel model)
        {
            return _classRepository.CreateClass(model);
        }

        [Route("GetListClassForCombo")]
        [HttpGet]
        public IActionResult GetListClassForCombo()
        {
            var comboData = _classRepository.GetListClassForCombo();
            return Ok(comboData);
        }
    }
}
