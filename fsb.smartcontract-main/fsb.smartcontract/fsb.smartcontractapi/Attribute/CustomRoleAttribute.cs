﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontractapi.Attribute
{
    public class CustomRoleAttribute : AuthorizeAttribute
    {
        public static string Status403Forbidden = "Bạn không có quyền thực hiện chức năng này!";
        public CustomRoleAttribute(params string[] roles)
        {
            Roles = String.Join(",", roles);
        }
    }
}

