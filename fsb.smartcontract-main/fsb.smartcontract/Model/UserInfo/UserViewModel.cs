﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UserInfo
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string UserRoleName { get; set; }
        public string UserCompanyCodes { get; set; }
        public string UserCompanyName { get; set; }
        public string UserDepartmentIds { get; set; }
        public string UserDepartmentName { get; set; }
        public string UserUnitName { get; set; }
        public string UserUnitCode { get; set; }
        public string UserRoleNormalizedName { get; set; }
        public bool IsAdmin { get; set; }
        public string Password { get; set; }
        public string UserProjectIds { get; set; }
        public string UserProjectName { get; set; }
        public bool IsUploadBudget { get; set; }
        public bool IsReportBudget { get; set; }
    }
}
