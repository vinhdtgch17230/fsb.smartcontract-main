﻿using fsb.smartcontract.Services.Company;
using fsb.smartcontract.Services.Department;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.Department;
using Model.Requests;
using Model.Response;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Controllers
{
    public class DepartmentController : BaseController
    {
        private readonly ILogger<DepartmentController> _logger;
        private readonly IToastNotification _toastNotification; //thông báo
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDepartmentService _departmentService;
        private readonly ICompanyService _companyService;

        public DepartmentController(
            ILogger<DepartmentController> logger,
            IHttpClientFactory factory,
            IToastNotification toastNotification,
            IHttpContextAccessor httpContextAccessor,
            IDepartmentService departmentService, //cầu nối gọi thằng API
            ICompanyService companyService
            ) : base(factory)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _httpContextAccessor = httpContextAccessor;
            _departmentService = departmentService;
            _companyService = companyService;
        }
        
        [Route("danh-sach-phong-ban")]
        public IActionResult Departments()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetListDepartment(string searchValue)
        {
            SearchDepartmentModel searchModel = talbeSearchModel.Get<SearchDepartmentModel>();
            searchModel.SearchValue = searchValue;
            var data = _departmentService.GetListDepartment(searchModel);
            if (data.Code != StatusCodes.Status200OK)
                _toastNotification.AddErrorToastMessage(data.Message);
            return Json(data);
        }

        [Route("them-moi-phong-ban")]
        public IActionResult CreateDepartment()
        {
            ViewBag.GetListCompanyForCombo = _companyService.GetListCompanyForCombo();
            return View();
        }

        [HttpPost]
        public IActionResult InsertDepartment(DepartmentModel model)
        {
            var res = _departmentService.CreateDepartment(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [HttpPost]
        public IActionResult DeleteDepartment(int Id)
        {
            var model = new DepartmentModel();
            model.Id = Id;
            var res = _departmentService.DeleteCompany(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [Route("sua-phong-ban/{id}")]
        //public IActionResult EditDepartment()
        public IActionResult EditDepartment(int Id)
        {
                ViewBag.GetListCompanyForCombo = _companyService.GetListCompanyForCombo();
            var model = new DepartmentModel();
            model.Id = Id;
            var res = _departmentService.GetDepartmentById(model);

            if (res == null)
                res = new Response<DepartmentModel>();
            if (res.Data == null)
                res.Data = new DepartmentModel();

            return View(res.Data);
        }

        [HttpPost]
        public IActionResult UpdateDepartment(DepartmentModel model)
        {
            var res = _departmentService.UpdateDepartment(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }
    }
}
